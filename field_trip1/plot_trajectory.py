import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import sys

if len(sys.argv) < 2:
    print 'Usage: python plot_trajectory.py filePath [pos_data] [square_axis]\n' \
          '\tpos_data (true - false): Use true when providing a pos.csv file in filePath (3D plot). Use false when' \
          ' providing a gps.csv file instead (2D plot). (default = true)\n' \
          '\tpos_data (true - false): Use true to use the same scale for X and Y when plotting the data. Use false' \
          ' for individual scaling factors. (default = true)'
    exit(1)

pos_data = True  # True: using pos.csv files from 'pos' folder. False: gps.csv files
square_axis = True  # True: Do not fit axis to data, instead use the same scale for X and Y
fname = sys.argv[1]
if len(sys.argv) > 2:
    pos_data = sys.argv[2] in ['true', 'True', '1']
if len(sys.argv) > 3:
    square_axis = sys.argv[3] in ['true', 'True', '1']

# Parse flight data
lines = [line.rstrip('\n') for line in open(fname, 'r')]
data = [line for line in lines[1:] if line.find(',') != -1]
x_gps = np.array([float(coord.split(', ')[0]) for coord in data]) * np.pi / 180.0
y_gps = np.array([float(coord.split(', ')[1]) for coord in data]) * np.pi / 180.0
if pos_data:
    alt = np.array([float(coord.split(', ')[2]) for coord in data])

# Convert GPS coordinates to meters
gps_zero = [min(x_gps), min(y_gps)]


def to_meters(d_lat, d_lon):
    R = 6371000  # metres
    a = np.sin(d_lat / 2) * np.sin(d_lat / 2) + np.cos(gps_zero[0]) * np.cos(x_gps) * np.sin(d_lon / 2) * np.sin(
        d_lon / 2)
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    return R * c


# Convert coordinates only for x (slight approximation)
d_x = to_meters(x_gps - gps_zero[0], 0)
# Convert coordinates only for y (slight approximation)
d_y = to_meters(0, y_gps - gps_zero[1])

# Plotting
fig = plt.figure()
if square_axis:
    # Square axis
    maxi = max(max(d_x), max(d_y))
    plt.xlim(-maxi / 10.0, 1.1 * maxi)
    plt.ylim(-maxi / 10.0, 1.1 * maxi)
else:
    # Stretch axis
    plt.xlim(-max(d_x) / 10, 1.1 * max(d_x))
    plt.ylim(-max(d_x) / 10, 1.1 * max(d_y))
plt.xlabel('x (m)')
plt.ylabel('y (m)')

if pos_data:
    # Plot for POS data
    ax = fig.gca(projection='3d')
    plt.plot(d_x, d_y, alt)
else:
    # Plot for GPS data
    plt.scatter(d_x, d_y)

plt.show()

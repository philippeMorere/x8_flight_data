# X8 Flight data #

### What is this repository for? ###

This repository contains the pixhawk logs gathered during the X8's first flight.

The logs are available in several formats:

* raw: All the information is available. The `.bin` files are to be open with a software like apmplanner2/ground control.
* gps: Only the gps information (2D) is available in `.csv` files.
* pos: Only the position information (3D) from the pixhawk is available in `.csv` files.

### Usage ###

The `plot_trajectory.py` script in the `field_trip1` folder can be used to visualize files from the `gps` or `pos` folders.